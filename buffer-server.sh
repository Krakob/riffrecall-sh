#!/usr/bin/env bash

# Source from the script's location
source "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/default-settings.sh"
source $HOME/.config/riffrecall.sh 2>/dev/null  # Silently fail if personal config doesn't exist
mkdir $OUTPUT_DIR 2>/dev/null # Make sure our output directory exists

function buffserv {
  if [ "$PLATFORM" == 'Linux' ]
  then
    ffmpeg -f alsa -i "$INPUT_DEVICE" -af aresample=async=1 -f segment -segment_time $BUFFER_LENGTH -segment_wrap 2 "$OUTPUT_DIR/.buffer%d.wav"
  elif [ "$PLATFORM" == 'Windows' ]
  then
    ffmpeg -f dshow -i audio="$INPUT_DEVICE" -af aresample=async=1 -f segment -segment_time $BUFFER_LENGTH -segment_wrap 2 "$OUTPUT_DIR/.buffer%d.wav"
  else
    echo "Unknown platform! Only Linux and Windows supported."
    exit 1
  fi
}

CRASH_COUNT=0;
while true
do
  STARTUP_TIME=$(date +%s)
  if [ $CRASH_COUNT -lt 3 ]
  then
    buffserv || if [ $(($(date +%s) - $STARTUP_TIME)) -lt $CRASH_INTERVAL ]
    then
      CRASH_COUNT=$((CRASH_COUNT+1))
    else
      continue
    fi
  else
    echo "Crash threshold reached! Exiting"
    notify-send "RiffRecall has crashed several times and is exiting."
    exit 1
  fi
done
