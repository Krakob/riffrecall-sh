#!/usr/bin/env bash

OUTPUT_DIR="$HOME/Music/RiffRecall"
NAME_FORMAT='RiffRecall_%Y-%m-%d_%H-%M-%S'
BUFFER_LENGTH=300  # Five minutes
CUT_BUFFER=true    # Trim output to buffer's length
MAX_CRASHES=3      # Let the program crash a maximum of three times
CRASH_INTERVAL=5   # in intervals of five seconds before giving up

# You should set the following manually in
# ~/.config/riffrecall.sh
# PLATFORM='Linux'
# INPUT_DEVICE='hw:1'
