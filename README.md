## RiffRecall

This is a simple wrapper program around ffmpeg used to keep an audio buffer of the last few minutes and then save said buffer at command. This allows you to run the program in the background constantly, and then save that awesome riff you just played after the fact!

Note that the program will not be able to recall riffs you played before you started the server, meaning that it's advisable to auto-start it with your PC and keep it in the background.

This program is very rough around the edges, you'll probably have to read the source code to troubleshoot things but feel free to post any questions as issues at the online repository. Pull requests welcome!


### Usage

Start by running `buffer-server.sh`. You should be able to run it in the background using `buffer-server.sh &`, or better yet, using GNU screen or Tmux.

Capture the current buffer with `capture-buffer.sh`.

Configuration is required. A default configuration file is provided. It is recommended that you do not change it, and instead write your own (which overwrites values in the default) at `~/.config/riffrecall.sh`. On Windows, this might be `C:\Users\<Your name>\.config/riffrecall.sh` but this will depend on which system you're running (cygwin will use another home directory by default, for example).


### Requirements

 * `bash` (through your system, Windows Subshell for Linux, or Cygwin)
 * `ffmpeg` (reasonably new version that supports segmentation)
 * `awk`
 * `date`

All these must be accessible on your $PATH, i.e. typing `ffmpeg` into your shell should successfully launch ffmpeg.


### License
MIT. See `LICENSE`


### TODO

 * Inspect argument flipping - `tac` needed on Ubuntu but not on Cygwin?
 * Implement trimming the final result to buffer length.
