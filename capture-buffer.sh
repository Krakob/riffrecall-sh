#!/usr/bin/env bash

source "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/default-settings.sh"
source $HOME/.config/riffrecall.sh 2>/dev/null  # Silently fail if personal config doesn't exist
mkdir $OUTPUT_DIR 2>/dev/null # Make sure our output directory exists
cd $OUTPUT_DIR

ffmpeg $(ls -At | tac | awk '/\.buffer[01].wav/ { printf " -i " $0 }') -filter_complex '[0:0][1:0]concat=n=2:v=0:a=1[out]' -map '[out]' $(date "+$NAME_FORMAT.wav") || (echo "RiffRecall capture failed!"; notify-send "RiffRecall capture failed!")
